package com.wlcb.jpower.module.common.enums;


/**
 * @Description //TODO 生成的随机数类型
 * @author mr.gmac
 */

public enum RandomType {
    /**
     * INT STRING ALL
     */
    INT, STRING, ALL
}
