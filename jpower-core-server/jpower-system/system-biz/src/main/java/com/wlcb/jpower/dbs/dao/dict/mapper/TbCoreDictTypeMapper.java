package com.wlcb.jpower.dbs.dao.dict.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.dbs.entity.dict.TbCoreDictType;
import org.springframework.stereotype.Component;

/**
 * @ClassName TbCoreParamsDao
 * @Description TODO
 * @Author 郭丁志
 * @Date 2020-07-03 13:30
 * @Version 1.0
 */
@Component("tbCoreDictTypeMapper")
public interface TbCoreDictTypeMapper extends BaseMapper<TbCoreDictType> {


}
