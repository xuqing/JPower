package com.wlcb.jpower.dbs.dao.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.dbs.entity.function.TbCoreDataScope;
import org.springframework.stereotype.Component;

/**
 * @author mr.gmac
 * @description
 * @date 2020-11-03 14:50
 */
@Component
public interface TbCoreDataScopeMapper extends BaseMapper<TbCoreDataScope> {
}
