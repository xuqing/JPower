package com.wlcb.jpower.dbs.dao.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.dbs.entity.client.TbCoreClient;
import org.springframework.stereotype.Component;

/**
 * @author mr.gmac
 */
@Component
public interface TbCoreClientMapper extends BaseMapper<TbCoreClient> {
}
