package com.wlcb.jpower.dbs.dao.city.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.dbs.entity.city.TbCoreCity;
import org.springframework.stereotype.Component;

/**
 * @ClassName TbCoreParamsDao
 * @Description TODO
 * @Author 郭丁志
 * @Date 2020-07-03 13:30
 * @Version 1.0
 */
@Component("tbCoreCityMapper")
public interface TbCoreCityMapper extends BaseMapper<TbCoreCity> {

}
